'use strict';

$(function () {
    var $page = $('html, body');
    $('.anchor').click(function () {
        $page.animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 50
        }, 400);
        return false;
    });
    $('.media__slider').slick({
        slidesToShow: 8,
        slidesToScroll: 1,
        infinite: true,
        arrows: true,
    });
    $('symbol[data-country]').mouseenter(function () {
        var country = $(this).attr('data-country');
        $('[data-country=' + country + ']').addClass('active');
    });
    $('symbol[data-country]').mouseleave(function () {
        var country = $(this).attr('data-country');
        $('[data-country=' + country + ']').removeClass('active');
    });
    $('symbol[data-country]').click(function () {
        var country = $(this).attr('data-country');
        $('path[data-country=' + country + ']').click();
    });
    $('[data-country="liechtenstein"]').click(function () {
        $.fancybox.open($('#liechtenstein-popup'));
    });
});

var a = 0;

$(window).scroll(function () {
    var header = $('#map').offset().top;
    if ($(window).scrollTop() >= header) {
        $('.header').addClass('header--fixed');
        $('.main').addClass('active');
        $('.section__decor-horizon').addClass('active');
    }
    else {
        if ($(window).scrollTop() == 0)  {
            $('.header').removeClass('header--fixed');
            $('.main').removeClass('active');
            $('.section__decor-horizon').removeClass('active');
        }
    }
    var oTop = $('.achiev__wrap').offset().top - window.innerHeight;
    if (a == 0 && $(window).scrollTop() >= oTop) {
        $('.achiev__number').each(function () {
            var $this = $(this),
                countTo = $this.attr('data-count');
            $({
                countNum: $this.text()
            }).animate({
                    countNum: countTo
                },
                {
                    duration: 2000,
                    easing: 'swing',
                    step: function () {
                        $this.text(Math.floor(this.countNum));
                    },
                    complete: function () {
                        $this.text(this.countNum);
                    }

                });
        });
        a = 1;
    }
});